
= Java Maven Template

Template project.

- JUnit 5
- Mockito
- Unit Test execution with Surefire
- Code coverage with JaCoCo (parameters in pom.xml)
- Checkstyle (Google style) - No javadoc (checkstyle-suppressions.xml)
- Sonar excludes - not with Maven build (sonar-project.properties)

Coverage is not measured in Main and in com.waghetti.example.java_maven_template.boundary.**.
