package com.waghetti.template.maven.java.message;

public class MessageHelloWorld implements Message {

  public String get() {
    return "Hello World";
  }

}
