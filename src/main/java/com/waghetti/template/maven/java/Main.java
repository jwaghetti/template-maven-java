
package com.waghetti.template.maven.java;

import com.waghetti.template.maven.java.boundary.io.OutFactory;
import com.waghetti.template.maven.java.message.MessageHelloWorld;
import com.waghetti.template.maven.java.message.Out;

public class Main {

  public static void main(String[] args) {
    Out out = OutFactory.create();
    MessageHelloWorld message = new MessageHelloWorld();

    out.sendMessage(message);
  }

}
