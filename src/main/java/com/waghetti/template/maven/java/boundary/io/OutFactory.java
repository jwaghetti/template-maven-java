package com.waghetti.template.maven.java.boundary.io;

import com.waghetti.template.maven.java.message.Out;

public class OutFactory {

  private static Out out;

  public static Out create() {
    if (out == null) {
      out = new StandardOut();
    }

    return out;
  }

}
