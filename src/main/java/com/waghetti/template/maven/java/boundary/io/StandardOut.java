
package com.waghetti.template.maven.java.boundary.io;

import com.waghetti.template.maven.java.message.Message;
import com.waghetti.template.maven.java.message.Out;

public class StandardOut implements Out {

  @Override
  public void sendMessage(Message message) {
    System.out.println(message.get());
  }

}
