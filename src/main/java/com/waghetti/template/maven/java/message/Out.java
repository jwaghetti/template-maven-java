package com.waghetti.template.maven.java.message;

public interface Out {
  void sendMessage(Message message);
}
