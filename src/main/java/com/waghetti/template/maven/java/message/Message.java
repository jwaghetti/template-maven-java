package com.waghetti.template.maven.java.message;

public interface Message {

  String get();

}
