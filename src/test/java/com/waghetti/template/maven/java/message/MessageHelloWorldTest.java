package com.waghetti.template.maven.java.message;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageHelloWorldTest {

  @Test
  void testHelloWorld() {
    assertEquals("Hello World", new MessageHelloWorld().get());
  }

}
