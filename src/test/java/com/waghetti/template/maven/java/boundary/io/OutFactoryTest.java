package com.waghetti.template.maven.java.boundary.io;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class OutFactoryTest {

  @Test
  void testStandardOut() {
    assertTrue(OutFactory.create() instanceof StandardOut);
  }


}
