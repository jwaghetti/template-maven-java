package com.waghetti.template.maven.java.message;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MockMessage {

 @Test
 void testMockMessage() {
   Message message = Mockito.mock(Message.class);
   Mockito.when(message.get()).thenReturn("Mock");
   assertEquals("Mock", message.get());
 }

}
